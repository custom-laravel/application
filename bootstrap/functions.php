<?php

if (!function_exists('escape_sql')) {
    /**
     * SQL構文文字列をエスケープします。
     *
     * @author tatsuki.kubota
     *
     * @param  string $val SQL文字列
     * @return string      SQL文字列
     */
    function escape_sql($val)
    {
        return str_replace(['\\', '%', '_'], ['\\\\', '\\%', '\\_'], $val);
    }
}

if (!function_exists('paginate')) {
    /**
     * ページネート情報を取得します。
     *
     * @author yujin.maruyama
     *
     * @param  int   $recordNum        開始レコード番号
     * @param  int   $totalRecordNum   全レコード数
     * @param  int   $perPageRecordNum 1ページ表示数
     * @param  int   $selectPageNum    選択可能なページ番号数
     * @return array                   ページネート情報
     */
    function paginate($recordNum, $totalRecordNum, $perPageRecordNum, $selectPageNum)
    {
        $response = [];

        // 現在ページの算出
        $current = ceil(($recordNum + 1) / $perPageRecordNum);
        $response += ['current' => $current];

        // 最初のページの算出
        if ($recordNum >= 1) {
            $response += ['first' => 0];
        }

        // 前ページの算出
        if ($recordNum >= 1) {
            $prevNum = $recordNum - $perPageRecordNum;
            if ($prevNum < 0) {
                $prevNum = 0;
            }
            $response += ['prev' => $prevNum];
        }

        // 次ページの算出
        if (($recordNum + $perPageRecordNum) <= $totalRecordNum - 1) {
            $response += ['next' => $recordNum + $perPageRecordNum];
        }

        // 最終ページの算出
        if (($recordNum + $perPageRecordNum) <= $totalRecordNum - 1) {
            $totalPageNum = ceil($totalRecordNum / $perPageRecordNum);
            $response += ['last' => $perPageRecordNum * $totalPageNum - $perPageRecordNum];
        }

        // 選択可能なページ番号数の算出
        if ($selectPageNum !== null) {
            $totalPageNum = ceil($totalRecordNum / $perPageRecordNum);
            $prevCurrentPageNum = ($selectPageNum - 1) / 2;
            $nextCurrentPageNum = $selectPageNum - $prevCurrentPageNum - 1;
            $startPageNum = $current - $prevCurrentPageNum;
            $endPageNum = $current + $nextCurrentPageNum;
            $select = [];
            if ($startPageNum <= 1) {
                $startPageNum = 1;
                $endPageNum = $selectPageNum < $totalPageNum ? $selectPageNum : $totalPageNum;
            } elseif ($endPageNum > $totalPageNum) {
                $startPageNum = $selectPageNum < $totalPageNum ? $totalPageNum - $selectPageNum + 1 : 1;
                $endPageNum = $totalPageNum;
            }
            for ($pageNum = $startPageNum; $pageNum <= $endPageNum; $pageNum++) {
                $tempRecordNum = $perPageRecordNum * ($pageNum - 1);
                $select += [$pageNum => $tempRecordNum];
            }
            $response += ['select' => $select];
        }

        return $response;
    }
}
