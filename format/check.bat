@echo off
REM --------------------------------------------------------------------------------
REM PHP-CS-Fixer フォーマットチェック
REM --------------------------------------------------------------------------------

cmd /c php ../vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix --config "../format/.php_cs" --dry-run --verbose --diff
pause