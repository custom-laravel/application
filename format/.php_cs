<?php

/**
 * @see https://mlocati.github.io/php-cs-fixer-configurator/
 */
return PhpCsFixer\Config::create()
    ->setUsingCache(false)
    ->setRiskyAllowed(false)
    ->setRules([
        '@PSR2'                                      => true,
        'line_ending'                                => false, // Gitにより文字コードが変換されている為、無効
        'psr0'                                       => false, // composer psr-4 を読み込まない為、無効
        'psr4'                                       => false, // composer psr-4 を読み込まない為、無効
        'binary_operator_spaces'                     => false, // 配列インデント整形が崩れる為、無効
        'class_keyword_remove'                       => false, // Laravelでは一般的に使用している為、無効
        'align_multiline_comment'                    => true,
        'array_indentation'                          => true,
        'array_syntax'                               => ['syntax'  => 'short'],
        'blank_line_after_opening_tag'               => true,
        'class_attributes_separation'                => ['elements' => ['const','method']],
        'concat_space'                               => ['spacing' => 'one'],
        'function_typehint_space'                    => true,
        'linebreak_after_opening_tag'                => true,
        'lowercase_static_reference'                 => true,
        'magic_constant_casing'                      => true,
        'method_separation'                          => true,
        'multiline_comment_opening_closing'          => true,
        'multiline_whitespace_before_semicolons'     => true,
        'native_function_casing'                     => true,
        'new_with_braces'                            => true,
        'no_blank_lines_after_class_opening'         => true,
        'no_blank_lines_after_phpdoc'                => true,
        'no_empty_comment'                           => true,
        'no_empty_phpdoc'                            => true,
        'no_empty_statement'                         => true,
        'no_leading_import_slash'                    => true,
        'no_leading_namespace_whitespace'            => true,
        'no_mixed_echo_print'                        => ['use' => 'echo'],
        'no_multiline_whitespace_before_semicolons'  => true,
        'no_singleline_whitespace_before_semicolons' => true,
        'no_spaces_around_offset'                    => ['positions' => ['outside']],
        'no_trailing_comma_in_singleline_array'      => true,
        'no_unused_imports'                          => true,
        'no_whitespace_in_blank_line'                => true,
        'normalize_index_brace'                      => true,
        'ordered_imports'                            => ['sort_algorithm' => 'alpha', 'imports_order' => ['class', 'const', 'function']],
        'phpdoc_align'                               => ['align' => 'vertical','tags' => ['param', 'throws', 'type']],
        'phpdoc_annotation_without_dot'              => true,
        'phpdoc_no_empty_return'                     => true,
        'phpdoc_no_package'                          => true,
        'phpdoc_scalar'                              => ['types' => ['boolean', 'double', 'integer', 'real', 'str']],
        'phpdoc_trim'                                => true,
        'phpdoc_types'                               => true,
        'single_blank_line_before_namespace'         => true,
        'single_line_comment_style'                  => true,
        'trailing_comma_in_multiline_array'          => true,
        'whitespace_after_comma_in_array'            => true,
        // 以下、setRiskyAllowedに起因する為、無効
        // 'ereg_to_preg'                               => true,
        // 'logical_operators'                          => true,
        // 'strict_comparison'                          => true,
    ])
    ->setFinder(PhpCsFixer\Finder::create()
        ->exclude('Model/Dao/')
        ->in(__DIR__ . '/../app')
    );
