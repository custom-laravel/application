@echo off
REM --------------------------------------------------------------------------------
REM PHP-CS-Fixer フォーマット
REM --------------------------------------------------------------------------------

cmd /c php ../vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix --config "../format/.php_cs" --verbose
pause