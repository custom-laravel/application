<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@getIndex');
Route::get('/home', 'HomeController@index');
Route::get('/maintenance', 'MaintenanceController@getIndex');
Route::get('/error', 'ErrorController@getIndex');

// Authentication Routes...
Route::get('login', 'LoginController@getIndex')->name('login');
Route::post('login', 'LoginController@postIndex');
Route::post('logout', 'LogoutController@postIndex')->name('logout');

// Registration Routes...
Route::get('register', 'RegisterController@getIndex')->name('register');
Route::post('register', 'RegisterController@postIndex');

// Password Reset Routes...
Route::get('password/forget', 'PasswordForgetController@getIndex')->name('password.request');
Route::post('password/forget', 'PasswordForgetController@postIndex')->name('password.email');
Route::get('password/reset/{token}', 'PasswordResetController@getIndex')->name('password.reset');
Route::post('password/reset', 'PasswordResetController@postIndex');
