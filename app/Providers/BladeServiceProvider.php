<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Bladeディレクティブ拡張 サービスプロバイダー
 *
 * @author yujin.maruyama
 */
class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        // Bladeインスタンス取得
        $blade = $this->app['view']->getEngineResolver()->resolve('blade')->getCompiler();
        // @set 変数定義拡張
        $blade->directive('set', function ($arg) {
            list($key, $value) = explode(',', $arg);
            return '<?php ' . $key . ' = ' . $value . '; ?>';
        });
        // @break 条件制御拡張
        $blade->directive('break', function ($arg) {
            return '<?php break; ?>';
        });
        // @continue 条件制御拡張
        $blade->directive('continue', function ($arg) {
            return '<?php continue; ?>';
        });
        // @asset フィンガープリントリンク作成
        $blade->directive('asset', function ($arg) {
            $file = str_replace(['\'', '"'], '', $arg);
            $opt = '';
            try {
                // ブラウザキャッシュ対策の為、PHPスクリプトを返却
                $opt = '?<?php echo \File::lastModified(\'' . public_path() . $file . '\'); ?>';
            } catch (\Exception $e) {
            }
            return secure_asset($file) . $opt;
        });
    }
}
