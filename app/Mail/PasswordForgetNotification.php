<?php

namespace App\Mail;

use Illuminate\Notifications\Notification as RootNotification;

/**
 * Password Forget Notification
 *
 * @author tatsuki.kubota
 */
class PasswordForgetNotification extends RootNotification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param mixed $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Notification())
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to($notifiable->email, $notifiable->name . '様')
            ->subject('【' . config('config.BASE_INFO.APP_NAME') . '】 パスワードリセットのお知らせ')
            ->text('mails.password_forget_notification', [
                'token'    => url(config('app.url') . route('password.reset', $this->token, false)),
                'userName' => $notifiable->name,
            ])
            ->withSwiftMessage(function (\Swift_Message $message) {
                $message
                ->setReturnPath(config('mail.from.address'))
                ->getHeaders()->addTextHeader('X-Mailer', 'header-mailer');
            });
    }
}
