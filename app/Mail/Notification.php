<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

/**
 * Notification class
 *
 * このbuildメソッドを実装したMailableクラスを用いて、
 * Notificationクラスにてメール通知を行う。
 *
 * @author tatsuki.kubota
 */
class Notification extends Mailable
{
    /**
     * Build the message.
     */
    public function build()
    {
    }
}
