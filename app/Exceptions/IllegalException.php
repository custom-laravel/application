<?php

namespace App\Exceptions;

use Exception;

/**
 * 処理実行時の異常終了を伴う事象のException
 *
 * @author tatsuki.kubota
 */
class IllegalException extends Exception
{
    /**
     * エラーコード定義
     * @var string
     */
    protected $errorCode = null;

    /**
     * IllegalException コンストラクタ
     *
     * @param string    $message   エラーメッセージ
     * @param stinrg    $errorCode エラーコード
     * @param Throwable $previous  Throw Exception
     */
    public function __construct(string $message = '', $errorCode = null, Throwable $previous = null)
    {
        $this->errorCode = $errorCode;
        parent::__construct($message, 1, $previous);
    }

    /**
     * エラーコードを取得します。
     *
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
}
