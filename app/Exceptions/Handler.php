<?php

namespace App\Exceptions;

use Container\Database\TimeoutException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Error code
     *
     * @var string
     */
    protected $errorCode = '';

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $e
     */
    public function report(Exception $e)
    {
        // エラーコード作成
        $baseErrorCode = date_format(date_create('now', timezone_open('Asia/Tokyo')), 'YmdHis');
        if ($e instanceof TokenMismatchException) {
            // POST通信によるCSRFトークン無効・セッションタイムアウトの場合
            $this->errorCode = 'T001-' . $baseErrorCode;
        } elseif ($e instanceof IllegalException) {
            // 予想済みエラー(不正操作など)の場合
            $this->errorCode = $e->getErrorCode() . '-' . $baseErrorCode;
        } elseif ($e instanceof TimeoutException) {
            // データベースアクセスタイムアウトの場合
            $this->errorCode = 'D001-' . $baseErrorCode;
        } else {
            // 上記以外
            $this->errorCode = 'A001-' . $baseErrorCode;
        }

        // ログ出力
        $messages = '';
        if ($e instanceof HttpException) {
            $messages .= 'HTTPエラー : ' . $e->getStatusCode();
        }
        \Log::error('エラー発生', [
            'ERROR_CODE'    => $this->errorCode,
            'ERROR_MESSAGE' => $e->getMessage() . $messages,
            'ERROR_TRACE'   => str_replace(["\n"], ' ', $e->getTraceAsString()),
        ]);
        // Laravel 基本ログ出力は使用しない
        // parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof HttpException && $e->getStatusCode() === 405) {
            // Routes定義なしの場合
            return redirect()->secure('/');
        } elseif ($e instanceof TokenMismatchException) {
            // POST通信によるCSRFトークン無効・セッションタイムアウトの場合
            return redirect()->back();
        } elseif ($request->expectsJson() && ($e instanceof AuthenticationException === false)) {
            // JSONリクエストの場合
            return response()->json(['error' => 'Internal Server Error.'], 500);
        }
        return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param \Illuminate\Http\Request                 $request
     * @param \Illuminate\Auth\AuthenticationException $e
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $e)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        return redirect()->guest(route('login'));
    }

    /**
     * Render the given HttpException.
     *
     * @param \Symfony\Component\HttpKernel\Exception\HttpException $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpException $e)
    {
        return redirect()->secure('/error')->with([
            'errorCode' => $this->errorCode,
        ]);
    }

    /**
     * Create a Symfony response for the given exception.
     *
     * @param \Exception $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertExceptionToResponse(Exception $e)
    {
        $e = FlattenException::create($e);

        if (config('app.debug', false)) {
            $handler = new SymfonyExceptionHandler(config('app.debug', false));
            return SymfonyResponse::create($handler->getHtml($e), $e->getStatusCode(), $e->getHeaders());
        } else {
            return redirect()->secure('/error')->with([
                'errorCode' => $this->errorCode,
            ]);
        }
    }
}
