<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

/**
 * ログ出力定義 ミドルウェア
 *
 * @author tatsuki.kubota
 */
class SetLog
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // ログ出力定義
        if (Auth::guard($guard)->check() && $request->path() !== 'logout') {
            $user = Auth::guard($guard)->authenticate();
            \Log::setValue([
                'TOKEN'           => substr($request->session()->getId(), 0, 6),
                'URL'             => $request->path(),
                'USER_ID'         => $user->id,
                'USER_NAME'       => $user->name,
            ]);
        } else {
            \Log::setValue([
                'TOKEN' => substr($request->session()->getId(), 0, 6),
                'URL'   => $request->path(),
            ]);
        }
        // 初期ログ出力
        \Log::info('HttpRequest - come');
        $response = $next($request);
        // 終了ログ出力
        if ($response instanceof RedirectResponse) {
            \Log::info('HttpRequest - redirect[' . $response->getTargetUrl() . ']');
        } else {
            \Log::info('HttpRequest - return');
        }
        return $response;
    }
}
