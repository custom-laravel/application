<?php

namespace App\Http\Middleware;

use Closure;

/**
 * メンテナンス確認 ミドルウェア
 *
 * @author tatsuki.kubota
 */
class CheckMaintenance
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('config.MAINTENANCE.FORCE', false)) {
            if ($request->expectsJson()) {
                return response()->json(['error' => 'Sorry. We\'re under maintenance.'], 503);
            } else {
                return redirect()->secure('/maintenance');
            }
        }
        return $next($request);
    }
}
