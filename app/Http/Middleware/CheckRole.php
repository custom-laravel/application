<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * 権限判定 ミドルウェア
 *
 * @author tatsuki.kubota
 */
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        $user = Auth::user();
        if ($user !== null && (in_array($user->role, explode(',', $role)) === false)) {
            return redirect()->secure('/');
        }
        return $next($request);
    }
}
