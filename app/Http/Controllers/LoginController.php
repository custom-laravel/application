<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

/**
 * Login Controller
 *
 * This controller handles authenticating users for the application and
 * redirecting them to your home screen. The controller uses a trait
 * to conveniently provide its functionality to your applications.
 *
 * @author tatsuki.kubota
 */
class LoginController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * the login username to be used by the controller.
     *
     * @var string
     */
    protected $username = 'email';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware([
            'maintenance',
        ]);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function postIndex(Request $request)
    {
        $this->validate($request, [
            'email'    => ['required', 'email', 'max:256'],
            'password' => ['required', 'regex:/^[A-Za-z\d_-]+$/', 'between:8,60'],
        ], [
            'email.required'    => 'メールアドレスを入力してください。',
            'email.email'       => 'メールアドレスの形式で入力してください。',
            'email.max'         => 'メールアドレスは:max文字以内で入力してください。',
            'password.required' => 'パスワードを入力してください。',
            'password.regex'    => 'パスワードは半角英数字,「-」,「_」で入力してください。',
            'password.between'  => 'パスワードは:min文字～:max文字で入力してください。',
        ]);

        $throttleKey = Str::lower($request->input($this->username)) . '|' . $request->ip();
        $rateLimiterInstance = app(RateLimiter::class);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($rateLimiterInstance->tooManyAttempts(
            $throttleKey,
                property_exists($this, 'maxAttempts') ? $this->maxAttempts : 5,
                property_exists($this, 'decayMinutes') ? $this->decayMinutes : 1
            )) {
            // fire lockout event
            event(new Lockout($request));

            // redirect the user after determining they are locked out.
            $seconds = $rateLimiterInstance->availableIn($throttleKey);
            $errors = [$this->username => Lang::get('auth.throttle', ['seconds' => $seconds])];

            if ($request->expectsJson()) {
                return response()->json($errors, 423);
            }

            return redirect()->back()
                ->withInput($request->only($this->username, 'remember'))
                ->withErrors($errors);
        }

        if (Auth::guard()->attempt($request->only($this->username, 'password'), $request->has('remember'))) {
            $request->session()->regenerate();

            // clear the login locks
            $rateLimiterInstance->clear($throttleKey);

            return $this->authenticated($request, Auth::guard()->user()) ?: redirect()->intended($this->redirectTo);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        // Increment the login attempts for the user.
        $rateLimiterInstance->hit($throttleKey);

        // failed login response
        $errors = [$this->username => trans('メールアドレス、またはパスワードが違います')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username, 'remember'))
            ->withErrors($errors);
    }

    /**
     * The user has been authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed                    $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
    }
}
