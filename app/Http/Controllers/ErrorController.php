<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

/**
 * Error Controller
 *
 * @author tatsuki.kubota
 */
class ErrorController extends Controller
{
    /**
     * Middleware定義
     */
    public function __construct()
    {
        $this->middleware([
            'maintenance',
        ]);
    }

    /**
     * GETメソッド処理
     *
     * @param Request $request
     * @param Guard   $auth
     * @return type
     */
    public function getIndex(Request $request, Guard $auth)
    {
        return response(view('error', [
            'errorCode' => $request->session()->get('errorCode'),
        ])->render(), 500);
    }
}
