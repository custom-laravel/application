<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

/**
 * Password Reset input Controller
 *
 * This controller is responsible for handling password reset emails and
 * includes a trait which assists in sending these notifications from
 * your application to your users. Feel free to explore this trait.
 *
 * @author tatsuki.kubota
 */
class PasswordForgetController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware([
            'maintenance',
            'guest'
        ]);
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('password_forget');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex(Request $request)
    {
        $this->validate($request, [
            'email'  => ['required', 'email', 'max:256'],
        ], [
            'email.required' => 'メールアドレスを入力してください。',
            'email.email'    => 'メールアドレスの形式で入力してください。',
            'email.max:256'  => 'メールアドレスは:256文字以内で入力してください。',
        ]);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = Password::broker()->sendResetLink(
            $request->only('email')
        );

        // send reset link
        if ($response === Password::RESET_LINK_SENT) {
            // success
            return back()->with('status', trans($response));
        } else {
            // failed
            return back()->withErrors(['email' => trans($response)]);
        }
    }
}
