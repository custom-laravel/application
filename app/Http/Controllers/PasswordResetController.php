<?php

namespace App\Http\Controllers;

use App\Model\Dao\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

/**
 * Password Reset Controller
 *
 * This controller is responsible for handling password reset requests
 * and uses a simple trait to include this behavior. You're free to
 * explore this trait and override any methods you wish to tweak.
 *
 * @author tatsuki.kubota
 */
class PasswordResetController extends Controller
{
    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware([
            'maintenance',
            'guest'
        ]);
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null              $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request, $token = null)
    {
        return view(
            'password_reset',
            [
                'token' => $token,
                'email' => $request->email,
            ]
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex(Request $request)
    {
        $this->validate($request, [
            'token'    => ['required'],
            'email'    => ['required', 'email', 'max:256'],
            'password' => ['required', 'regex:/^[A-Za-z\d_-]+$/', 'between:8,60', 'confirmed'],
        ], [
            'token.required'     => 'メールに記載されたURLからアクセスしてください。',
            'email.required'     => 'メールアドレスを入力してください。',
            'email.email'        => 'メールアドレスは正しい形式で入力してください。',
            'email.max:256'      => 'メールアドレスは:256文字以内で入力してください。',
            'password.required'  => 'パスワードを入力してください。',
            'password.regex'     => 'パスワードは半角英数字,「-」,「_」で入力してください。',
            'password.confirmed' => 'パスワードが再入力と一致しません。',
            'password.between'   => 'パスワードは:min文字以上:max以下で入力してください。',
        ]);

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = Password::broker()->reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                // reset password
                Users::updatePassword($user->id, bcrypt($password), Str::random(60));
                Auth::guard()->login($user);
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        if ($response === Password::PASSWORD_RESET) {
            return redirect($this->redirectTo)
                ->with('status', trans($response));
        } else {
            return redirect()->back()
                ->withInput($request->only('email'))
                ->withErrors(['email' => trans($response)]);
        }
    }
}
