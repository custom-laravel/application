<?php

namespace App\Http\Controllers;

use App\Model\Dao\Users;
use Container\Auth\CustomUser;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Register Controller
 *
 * This controller handles the registration of new users as well as their
 * validation and creation. By default this controller uses a trait to
 * provide this functionality without requiring any additional code.
 *
 * @author tatsuki.kubota
 */
class RegisterController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware([
            'maintenance',
            'guest'
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postIndex(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $requestParam = $request->all();

        Users::insertUsers(
            $requestParam['name'],
            $requestParam['email'],
            bcrypt($requestParam['password']),
            substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, 30)
        );
        $user = new CustomUser(Users::selectEmail($requestParam['email'])[0], null);

        event(new Registered($user));

        Auth::guard()->login($user);

        return redirect()->secure($this->redirectTo);
    }
}
