<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

/**
 * Maintenance Controller
 *
 * @author tatsuki.kubota
 */
class MaintenanceController extends Controller
{
    /**
     * Middleware定義
     */
    public function __construct()
    {
    }

    /**
     * GETメソッド処理
     *
     * @param \Illuminate\Http\Request         $request
     * @param \Illuminate\Contracts\Auth\Guard $auth
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request, Guard $auth)
    {
        return view('maintenance');
    }
}
