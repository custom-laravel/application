<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

/**
 * Index Controller
 *
 * @author tatsuki.kubota
 */
class IndexController extends Controller
{
    /**
     * Middleware定義
     */
    public function __construct()
    {
        // none middleware
    }

    /**
     * GETメソッド処理
     *
     * @param Request $request
     * @param Guard   $auth
     * @return type
     */
    public function getIndex(Request $request, Guard $auth)
    {
        return view('welcome');
    }
}
