<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

/**
 * Home Controller (TEST)
 *
 * ログイン処理確認用のログイン画面です。
 * 適宜変更・削除してください。
 *
 * @author tatsuki.kubota
 */
class HomeController extends Controller
{
    /**
     * Middleware定義
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * GETメソッド処理
     *
     * @param Request $request
     * @param Guard   $auth
     * @return type
     */
    public function index(Request $request, Guard $auth)
    {
        return view('home');
    }
}
