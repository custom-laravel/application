<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Logout Controller
 *
 * @author tatsuki.kubota
 */
class LogoutController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        // none middleware
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postIndex(Request $request)
    {
        Auth::guard()->logout();

        $request->session()->invalidate();

        return redirect()->secure('/');
    }
}
