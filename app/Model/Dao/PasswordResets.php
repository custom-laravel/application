<?php

namespace App\Model\Dao;

use Container\Support\Dao\PasswordResets as PasswordResetsInterfase;
use Container\Support\Facades\DB;

/**
 * パスワードリセット DAOクラス
 *
 * @author tatsuki.kubota
 */
class PasswordResets implements PasswordResetsInterfase
{
    /**
     * 対象のリセットトークンを削除します。
     *
     * @param  string $email メールアドレス
     * @return array         クエリ結果
     */
    public static function selectEmail($email)
    {
        return DB::select("
            SELECT
                id,
                email,
                token,
                created_at
            FROM
                password_resets
            WHERE
                email = :email
        ", [
            ':email' => $email,
        ]);
    }

    /**
     * 対象のリセットトークンを削除します。
     *
     * @param  string $email メールアドレス
     * @return int           クエリ結果
     */
    public static function deleteExisting($email)
    {
        return DB::delete("
            DELETE
            FROM
                password_resets
            WHERE
                email = :email
            ", [
                ':email' => $email,
            ]);
    }

    /**
     * 期限切れのリセットトークンを削除します。
     *
     * @param  string $expiredAt 有効期限
     * @return int               クエリ結果
     */
    public static function deleteExpired($expiredAt)
    {
        return DB::delete("
            DELETE
            FROM
                password_resets
            WHERE
                created_at < :expired_at
            ", [
                ':expired_at' => $expiredAt,
            ]);
    }

    /**
     * リセットトークンを作成します。
     *
     * @param  string $email     メールアドレス
     * @param  string $token     一時記憶トークン
     * @param  string $createdAt 作成日時
     * @return int               クエリ結果
     */
    public static function insertResetToken($email, $token, $createdAt)
    {
        return DB::insert("
            INSERT INTO password_resets(
                email,
                token,
                created_at
            )
            VALUES(
                :email,
                :token,
                :created_at
            )
            ", [
                ':email'      => $email,
                ':token'      => $token,
                ':created_at' => $createdAt,
            ]);
    }
}
