<?php

namespace App\Model\Dao;

use Container\Support\Facades\DB;

/**
 * ファイル DAOクラス
 *
 * @author tatsuki.kubota
 */
class Files
{
    /**
     * ファイル情報を登録します。
     *
     * @param  string $name      ファイル名
     * @param  string $mimetype  MIMEタイプ
     * @param  string $extention ファイル拡張子
     * @param  string $disk      ディスク
     * @param  string $filePath  ファイルパス
     * @param  int    $byteSize  ファイルサイズ
     * @param  int    $isTemp    一時ファイル判定
     * @return int               実行結果
     */
    public static function insertFiles($name, $mimetype, $extention, $disk, $filePath, $byteSize)
    {
        return DB::insert("
            INSERT INTO files(
                name,
                mimetype,
                extention,
                disk,
                file_path,
                byte_size,
                created_at,
                updated_at
            )
            VALUES(
                :name,
                :mimetype,
                :extention,
                :disk,
                :file_path,
                :byte_size,
                NOW(),
                NOW()
            )
            ", [
                ':name'      => $name,
                ':mimetype'  => $mimetype,
                ':extention' => $extention,
                ':disk'      => $disk,
                ':file_path' => $filePath,
                ':byte_size' => $byteSize,
            ]);
    }

    /**
     * ファイルIDよりファイル情報を削除します
     *
     * @param  string $id   ファイルID
     * @param  string $disk ディスク
     * @return int          実行結果
     */
    public static function updateDisk($id, $disk)
    {
        return DB::update("
            UPDATE
                files
            SET
                disk = :disk
            WHERE
                id IN (:id)
            ", [
                ':id'   => $id,
                ':disk' => $disk,
            ]);
    }

    /**
     * ファイルIDよりファイル情報を削除します
     *
     * @param  string $id ファイルID
     * @return int        実行結果
     */
    public static function deleteId($id)
    {
        return DB::delete("
            DELETE
            FROM
                files
            WHERE
                id IN (:id)
            ", [
                ':id' => $id,
            ]);
    }

    /**
     * 指定のレコード作成日時より古い一時保存ファイル情報を取得します
     *
     * @param  string $targetDate 指定日時
     * @return array              クエリ結果
     */
    public static function selectTempOldCreateDate($targetDate)
    {
        return DB::select("
            SELECT
                id,
                name,
                mimetype,
                extention,
                disk,
                file_path,
                byte_size
            FROM
                files
            WHERE
                disk = 'temp'
                AND created_at <= :target_date
            ", [
                ':target_date' => $targetDate,
            ]);
    }

    /**
     * ファイルIDよりファイル情報を取得します
     *
     * @param  string|array $id ファイルID
     * @return array            クエリ結果
     */
    public static function selectId($id)
    {
        if (empty($id)) {
            return null;
        }
        return DB::select("
            SELECT
                id,
                name,
                mimetype,
                extention,
                disk,
                file_path,
                byte_size
            FROM
                files
            WHERE
                id IN ( :id )
            ", [
                ':id' => $id,
            ]);
    }
}
