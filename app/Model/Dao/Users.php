<?php

namespace App\Model\Dao;

use Container\Support\Dao\Users as UsersInterfase;
use Container\Support\Facades\DB;

/**
 * ユーザ DAOクラス
 *
 * @author tatsuki.kubota
 */
class Users implements UsersInterfase
{
    /**
     * ユーザIDよりユーザ情報を取得します。
     *
     * @param  string $id ユーザID
     * @return array      クエリ結果
     */
    public static function selectId($id)
    {
        return DB::select("
            SELECT
                U.id,
                U.name,
                U.email,
                U.password,
                U.remember_token,
                U.api_token
            FROM
                users AS U
            WHERE
                U.id = :id
            ", [
                ':id' => $id,
            ]);
    }

    /**
     * メールアドレスよりユーザ情報を取得します。
     *
     * @param  string $email メールアドレス
     * @return array         クエリ結果
     */
    public static function selectEmail($email)
    {
        return DB::select("
            SELECT
                id,
                name,
                email,
                password,
                remember_token,
                api_token,
                role
            FROM
                users
            WHERE
                email = :email
            ", [
                ':email' => $email,
            ]);
    }

    /**
     * APIトークンよりユーザ情報を取得します。
     *
     * @param  string $apiToken APIトークン
     * @return array            クエリ結果
     */
    public static function selectApiToken($apiToken)
    {
        return DB::select("
            SELECT
                id,
                name,
                email,
                password,
                remember_token,
                role
            FROM
                users
            WHERE
                api_token = :api_token
            ", [
                ':api_token' => $apiToken,
            ]);
    }

    /**
     * 一時記憶トークンよりユーザ情報を取得します。
     *
     * @param  string $id            ユーザID
     * @param  string $rememberToken 一時記憶トークン
     * @return array                 クエリ結果
     */
    public static function selectRememberToken($id, $rememberToken)
    {
        return DB::select("
            SELECT
                id,
                name,
                email,
                password,
                remember_token,
                api_token,
                role
            FROM
                users
            WHERE
                id = :id
                AND remember_token = :remember_token
            ", [
                ':id'             => $id,
                ':remember_token' => $rememberToken,
            ]);
    }

    /**
     * 一時記憶トークンを更新します。
     *
     * @param  string $id            ユーザID
     * @param  string $rememberToken 一時記憶トークン
     * @return int
     */
    public static function updateRememberToken($id, $rememberToken)
    {
        return DB::update("
            UPDATE
                users
            SET
                remember_token = :remember_token,
                updated_at = NOW()
            WHERE
                id = :id
            ", [
                ':id'             => $id,
                ':remember_token' => $rememberToken,
            ]);
    }

    /**
     * パスワードを更新します。
     *
     * @param  string $id            ユーザID
     * @param  string $password      パスワード
     * @param  string $rememberToken 一時記憶トークン
     * @return int
     */
    public static function updatePassword($id, $password, $rememberToken)
    {
        return DB::update("
            UPDATE
                users
            SET
                password = :password,
                remember_token = :remember_token,
                updated_at = NOW()
            WHERE
                id = :id
            ", [
                ':id'             => $id,
                ':password'       => $password,
                ':remember_token' => $rememberToken,
            ]);
    }

    /**
     * ユーザを登録します。
     *
     * @param  string $name
     * @param  string $email
     * @param  string $password
     * @param  string $apiToken
     * @param  string $role
     * @return int
     */
    public static function insertUsers($name, $email, $password, $apiToken, $role = null)
    {
        return DB::insert("
            INSERT INTO users(
                name,
                email,
                password,
                api_token,
                role,
                created_at,
                updated_at
            )
            VALUES(
                :name,
                :email,
                :password,
                :api_token,
                :role,
                NOW(),
                NOW()
            )
            ", [
                ':name'      => $name,
                ':email'     => $email,
                ':password'  => $password,
                ':api_token' => $apiToken,
                ':role'      => $role,
            ]);
    }

    /**
     * ユーザを更新します。
     *
     * @param  string $userId
     * @param  string $name
     * @param  string $email
     * @param  string $role
     * @return int
     */
    public static function updateId($userId, $name, $email, $role = null)
    {
        return DB::insert("
            UPDATE
                users
            SET
                name = :name,
                email = :email,
                role = :role,
                updated_at = NOW()
            WHERE
                id = :user_id
            ", [
                'user_id'    => $userId,
                ':name'      => $name,
                ':email'     => $email,
                ':role'      => $role,
            ]);
    }
}
