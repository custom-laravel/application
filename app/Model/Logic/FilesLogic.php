<?php

namespace App\Model\Logic;

use App\Model\Dao\Files;
use DB;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * ファイル Logic
 *
 * @author tatsuki.kubota
 */
class FilesLogic
{
    /**
     * ファイルを保存します。
     *
     * @param UploadedFile $file ファイル
     * @param string       $disk ストレージディスク
     * @return string             ファイルID
     */
    public static function setFile(UploadedFile $file, $disk)
    {
        // ファイル格納 + データ登録
        $storage = Storage::disk($disk);
        Files::insertFiles(
            $file->getClientOriginalName(),
            $file->getClientMimeType(),
            $file->getClientOriginalExtension(),
            $disk,
            $storage->putFile('', $file),
            $file->getClientSize()
        );
        return DB::getLastInsertId();
    }

    /**
     * ファイルを削除します。
     *
     * @param string|array $id ファイルID
     */
    public static function deleteFile($id)
    {
        $filesInfo = Files::selectId($id);
        if (empty($filesInfo)) {
            return;
        }
        // ファイル削除
        foreach ($filesInfo as $value) {
            Storage::disk($value['disk'])->delete($value['file_path']);
        }
        // データ削除
        Files::deleteId($id);
    }

    /**
     * ファイルを指定ディスクに移動します。
     *
     * @param string|array $id       ファイルID
     * @param string       $sendDisk ディスク
     */
    public static function moveDisk($id, $sendDisk)
    {
        // ファイル移動
        foreach (Files::selectId($id) as $value) {
            $fromStorage = Storage::disk($value['disk']);
            if ($fromStorage->exists($value['file_path']) === false) {
                continue;
            }
            Storage::disk($sendDisk)->putFileAs('', new File($fromStorage->path('') . $value['file_path']), $value['file_path']);
            $fromStorage->delete($value['file_path']);
        }
        // データ更新
        Files::updateDisk($id, $sendDisk);
    }

    /**
     * リクエストされたファイルを一時保存します。
     *
     * 以下のリクエストパラメータキーが追加されます。
     * ・リクエストパラメータキー + 「_id」にファイルID
     * ・リクエストパラメータキー + 「_name」にファイル名が格納されます。
     *
     * @param Request $request リクエスト
     */
    public static function saveTempFile(Request $request)
    {
        foreach ($request->allFiles() as $key => $file) {
            $storage = Storage::disk('temp');
            Files::insertFiles(
                $file->getClientOriginalName(),
                $file->getClientMimeType(),
                $file->getClientOriginalExtension(),
                'temp',
                $storage->putFile('', $file),
                $file->getClientSize()
            );
            $filesId = DB::getLastInsertId();
            if (empty($request->input($key . '_id')) === false) {
                // 旧アップロードファイル削除
                FilesLogic::deleteFile($request->input($key . '_id'));
            }
            $request->offsetSet($key . '_id', $filesId);
            $request->offsetSet($key . '_name', $file->getClientOriginalName());
        }
    }

    /**
     * パラメータより一時保存ファイル情報を取得します。
     *
     * @param Request $request リクエスト
     * @param string  $param   パラメータ名
     * @return array
     */
    public static function getTempFileInfo(Request $request, $param)
    {
        $filesInfo = Files::selectId($request->input($param . '_id', ''));
        if (isset($filesInfo[0])) {
            return $filesInfo[0];
        }
        return null;
    }

    /**
     * ファイル情報が画像形式であるかチェックします。
     *
     * @param array $filesInfo ファイル情報
     * @return bool            バリデート結果
     */
    public static function validateImage($filesInfo)
    {
        if (empty($filesInfo)) {
            return false;
        }
        $parameters = ['jpeg', 'png', 'gif', 'bmp', 'svg'];
        return in_array($filesInfo['mimetype'], $parameters) ||
            in_array(explode('/', $filesInfo['mimetype'])[0] . '/*', $parameters);
    }

    /**
     * 画像ファイルURLを取得します。
     *
     * @param string $path ファイルパス
     * @param string $disk ディスク
     * @return string      URL
     */
    public static function getImageUrl($path, $disk)
    {
        if (empty($path)) {
            return config('config.DEFAULT_IMAGE_URL');
        }
        return Storage::disk($disk)->url($path);
    }
}
