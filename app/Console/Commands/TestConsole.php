<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * Test Console (TEST)
 *
 * コマンド検証用の処理です。
 * 適宜変更・削除してください。
 *
 * @author tatsuki.kubota
 */
class TestConsole extends Command
{
    /**
     * コンソールコマンドの名前と引数、オプション
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * コンソールコマンドの説明
     *
     * @var string
     */
    protected $description = 'test console';

    /**
     * コンソールコマンドの実行
     *
     * @return mixed
     */
    public function handle()
    {
        // ログ定義
        \Log::setValue([
            'CMD' => $this->argument(),
        ]);
        \Log::error('----- TEST CONSOLE -----');
    }
}
