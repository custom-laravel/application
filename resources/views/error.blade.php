@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Error</div>

                <div class="panel-body">
                    @if (empty($errorCode) === false)
                        <b>ErrorCode : {{ $errorCode }}</b><br>
                    @endif
                    Error occurred.<br>
                    Please, try again from <a href="/">Top Page</a> again.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
