@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Maintenance</div>

                <div class="panel-body">
                    Maintenance Now.<br>
                    Please, wait time.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
