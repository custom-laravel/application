<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'パスワードは8文字～60文字でパスワード再入力と同じものを入力してください。',
    'reset'    => 'パスワードを再設定しました。',
    'sent'     => 'パスワード再設定のリンクを送信しました。',
    'token'    => 'メールアドレスが違います。',
    'user'     => 'メールアドレスが違います。',
];
